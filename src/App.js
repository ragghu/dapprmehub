import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import login from './Authentication/login'
import home from './Dashboard/home';
import usermanagement from './Dashboard/usermanagement'
import leadmanagement from './Dashboard/leadmanagement';
import ticketmanagement from './Dashboard/ticketmanagement';
import eventmanagement from './Dashboard/eventmanagement';
class App extends Component {
  render() {
    return (
      <Router>
      <Switch>
        <Route exact path="/" component={login}></Route>
        <Route exact path="/home" component={home}></Route>
        <Route exact path='/admin/usermanagement' component={usermanagement}></Route>
        <Route exact path='/admin/leadmanagement' component={leadmanagement}></Route>
        <Route exact path='/admin/ticketmanagement' component={ticketmanagement}></Route>
        <Route exact path='/admin/eventmanagement' component={eventmanagement}></Route>
      </Switch>
    </Router>
    );
  }
}

export default App;
