import React, { Component } from 'react';
import {
    Icon, Input, Button, Row, Col, Card
} from 'antd'
import '../App.css'
class login extends Component {
    constructor() {
        super();
        this.state = {
       username:"",
       password:"",
       btndisabled:true
        };

    }
    usernamehandler=(e)=>{
        console.log("user",e.target.value)
        this.setState({username:e.target.value})
        if(this.state.password!="" && e.target.value!=""){
            this.setState({btndisabled:false})
        }
        else{
            this.setState({btndisabled:true})
        }
    }
    userpasswordhandler=(e)=>{
        console.log("password",e.target.value)
        this.setState({password:e.target.value})
        if(this.state.username!="" && e.target.value!=""){
            this.setState({btndisabled:false})
        }
        else{
            this.setState({btndisabled:true})
        }
    }
    submitHandler=()=>{
        this.props.history.push('/home');
    }
    render() {
        return (
            <div>
                <div
                    style={{ textAlign: "center", margin: "125px 10px 10px 10px", }}>
                    <img src="images/logo.png" style={{ width: 200 }} />
                </div>
                <div className="loginmaindiv">
                    <Card>
                        <Row>
                            <Col lg={{ span: 24 }}>
                                <Input onChange={this.usernamehandler} size="large" prefix={<Icon type="user" 
                                style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" 
                                value={this.state.username}
                                />
                            </Col>
                        </Row>
                        <div style={{ marginTop: 20 }}>
                            <Row>
                                <Col lg={{ span: 24 }}>
                                    <Input onChange={this.userpasswordhandler} size="large" prefix={<Icon type="lock"
                                     style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" 
                                     value={this.state.password}
                                     />
                                </Col>
                            </Row>
                        </div>
                        <div style={{ marginTop: 20 }}>
                            <Button size="large" type="primary" htmlType="submit" disabled={this.state.btndisabled}
                             className="login-form-button" onClick={this.submitHandler}>
                                Log in
          </Button>
                        </div>
                    </Card>
                </div>
            </div>
        );
    }
}

export default login
