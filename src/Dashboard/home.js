import React, { Component } from 'react';
import {
    CardImage, Row, Col, Card, CardBody, CardText
} from 'antd'
import '../App.css'
const { Meta } = Card;

class home extends Component {
    constructor() {
        super();
        this.state = {

        };

    }
    routetoUsermanagement = () => {
        this.props.history.push('/admin/usermanagement')

    }
    routetoLeadmanagement=()=>{
        this.props.history.push('/admin/leadmanagement')
    }
    routetoTicketmanagement=()=>{
        this.props.history.push('/admin/ticketmanagement')
    }
    routetoEventmanagement=()=>{
        this.props.history.push('/admin/eventmanagement')
    }
    render() {
        return (
            <div className="container-fluid">
                <div>
                    <Row>
                        
                        <Col lg={{ span: 6 }}>
                        <div className="routeCradDiv">
                            <Card 
                               onClick={this.routetoUsermanagement}
                                hoverable
                                cover={<img className="cardimage" alt="example" src="/images/usermanagement.jpg" />}
                            >
                                <Meta
                                    title="User Management"
                                />
                            </Card>
                            </div>
                        </Col>
                        
                        <Col lg={{ span: 6 }}>
                        <div className="routeCradDiv">
                            <Card
                            onClick={this.routetoLeadmanagement}
                                hoverable
                                cover={<img className="cardimage" alt="example" src="/images/leadmanagement.png" />}
                            >
                                <Meta
                                    title="Lead Management"
                                />
                            </Card>
                            </div>
                        </Col>
                        
                        
                        <Col lg={{ span: 6 }}>
                        <div className="routeCradDiv">
                            <Card
                            onClick={this.routetoTicketmanagement}
                                hoverable
                                cover={<img alt="example" className="cardimage" src="/images/ticket2.png" />}
                            >
                                <Meta
                                    title="Ticket Management"
                                />
                            </Card>
                            </div>
                        </Col>
                        
                        
                        <Col lg={{ span: 6 }}>
                        <div className="routeCradDiv">
                            <Card
                            onClick={this.routetoEventmanagement}
                                hoverable
                                cover={<img className="cardimage" alt="example" src="/images/event.png" />}
                            >
                                <Meta
                                    title="Event & Announcements"
                                />
                            </Card>
                            </div>
                        </Col>
                        
                    </Row>
                </div>
            </div>
        );
    }
}

export default home
